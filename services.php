<?php $base = strstr( $_SERVER[ 'HTTP_HOST' ], 'localhost' ) || strstr( $_SERVER[ 'HTTP_HOST' ], '127.0.0.1' ) ? '/development/portfolio/' : '/'; ?>
<?php ob_start(); ?>
<?php $head_js_files = array( ); ?>
<?php $body_js_files = array( 'bootstrap-modal','bootstrap-tab','services' ); ?>
<?php $page = 'services'; ?>
<?php $keywords = 'services,options,what we can do'; ?>
<?php $title = 'Services'; ?>
    <div class="content-wrapper">
        <section class="row-fluid">
            <?php $services = array(
                                  'PHP Consulting' => 'consulting',
                                  'Project Management' => 'project_management',
                                  'Custom Applications' => 'custom_applications',
                                    'Add-on Development' => 'addons',
                                    'Graphic Design' => 'design',
                                    'Web Development' => 'web_development'
                              );
            ?>
            <?php foreach( $services as $service => $anchor): ?>
            <div class="span2 f50 text-center">
                <a href="#<?php echo $anchor; ?>"><?php echo $service; ?></a>
            </div>
            <?php endforeach; ?>
        </section>
        <section class="row-fluid">
            <article class="span8">
            <div class="padding">
                <h1>Our Services</h1>
                <div class="box2 relative">
                    <div class="span1 hidden-phone">
                        <img src="img/marker_1.gif">
                    </div>
                    <div class="span11">
                        <a name="consulting"></a>
                        <h2>Expert PHP Consulting</h2>
                        <p>You can't always do it yourself. Sometimes you need a little help. Let us give you a hand. We have worked on all kinds of systems and projects and have seen almost all there is to be seen. With over a decade of experience, we will help you achieve your goals.</p>
                        <div class="margin-bottom center">
                            <a href="#" class="button" data-toggle="modal" data-target="#consulting-more"><span><span>Read More</span></span></a>
                        </div>
                        <div class="clear">&nbsp;</div>
                        <div id="consulting-more" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
                                <h2 id="myModalLabel">Expert PHP Consulting</h2>
                            </div>
                            <div class="modal-body">
                                <p>Do you have a new web idea or project and not sure on where to start? Or maybe you already have an existing website, but are experiencing lower than normal performance or effectiveness?</p>
                                <p>We at J&amp;L Core Systems offers full PHP Consulting to both individuals and companies alike. Whether you are looking for a way to start up your idea, project or venture or to enhance and improve your current website, we are here to help.</p>
                                <p>We have over 10 years of industry experience in PHP development which has given us the insights and resources to maximize web effectiveness. We can help you realize the steps necessary to make your project a reality.</p>
                                <p>We will work together with you to understand your needs and come up with an appropriate, smart solution that works for you. We can help you to understand what is involved, what will be needed, and how to get the ball rolling quickly and efficiently. Whether you need us to help plan and setup or want us to oversee the entire process from planning to successful launch, we will be there for you.</p>
                                <p>We understand that you may not be technical yourself, so we will ensure that we convey ourselves in such a way that you will understand every step and every process.</p>
                            </div>
                            <div class="modal-footer title2">
                                <h2 class="pull-left"><a class="" href="<?php echo $base; ?>contact">Contact Us Today</a></h2>
                                <button class="btn" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="box2">
                    <div class="span1 hidden-phone">
                        <img src="img/marker_1.gif">
                    </div>
                    <div class="span11">
                        <a name="project_management"></a>
                        <h2>Project Management</h2>
                        <p>Have an idea and need help with getting it going? Let us help. We can help you to plan out your project from start to finish and help with all the details along the way. We have experience with all types of projects large and small. So don't hesitate to ask us for assistance.</p>
                        <div class="margin-bottom center">
                            <a href="#" class="button" data-toggle="modal" data-target="#project_management-more"><span><span>Read More</span></span></a>
                        </div>
                        <div class="clear">&nbsp;</div>
                        <div id="project_management-more" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
                                <h2 id="myModalLabel">Project Management</h2>
                            </div>
                            <div class="modal-body">
                                <p>
                                    Sometimes you need someone with the expertise and the knowledge of the steps needed to plan, build, test, and launch a project. We can oversee your project from start to finish and ensure that everything goes as planned.
                                </p>
                                <p>
                                    We have planned and developed numerous projects small and large over the years and will can ensure that your project will have the best possible chances of success.
                                </p>
                            </div>
                            <div class="modal-footer title2">
                                <h2 class="pull-left"><a class="" href="<?php echo $base; ?>contact">Contact Us Today</a></h2>
                                <button class="btn" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="box2">
                    <div class="span1 hidden-phone">
                        <img src="img/marker_1.gif">
                    </div>
                    <div class="span11">
                        <a name="custom_applications"></a>
                        <h2>Custom Applications</h2>
                        <p>Do you have a unique idea that you would like to see developed? Let us help you plan, build, test and deploy your idea. Use our experience in project management, design and development to turn your ideas into reality.</p>
                        <div class="margin-bottom center">
                            <a href="#" class="button" data-toggle="modal" data-target="#custom_applications-more"><span><span>Read More</span></span></a>
                        </div>
                        <div class="clear">&nbsp;</div>
                        <div id="custom_applications-more" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
                                <h2 id="myModalLabel">Custom Applications</h2>
                            </div>
                            <div class="modal-body">
                                <p>There isn't always something out there that's readily available to suit your needs. You may need complete website development (See our website development service), or an add-on to an existing site (see our add-on services), and sometimes you just need some custom code or a page that does a specific task.</p>
                                <p>Regardless of what you need done, if it's possible, we can help.</p>
                                <div class="title2 text-center"><h1>If you can dream it, we can build it!</h1></div>
                            </div>
                            <div class="modal-footer title2">
                                <h2 class="pull-left"><a class="" href="<?php echo $base; ?>contact">Contact Us Today</a></h2>
                                <button class="btn" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="box2">
                    <div class="span1 hidden-phone">
                        <img src="img/marker_1.gif">
                    </div>
                    <div class="span11">
                        <a name="addons"></a>
                        <h2>Add-On Development</h2>
                        <p>Already have an existing website? Does it use a current PHP framework such as Wordpress, Symfony, Zend Framework, Magneto, Drupal, or another? If you need more functionality or features for your site, let us help. We have over a decade of experience with PHP frameworks and know how they work and how to add the functionality that you want.</p>
                        <div class="margin-bottom center">
                            <a href="#" class="button" data-toggle="modal" data-target="#addon-more"><span><span>Read More</span></span></a>
                        </div>
                        <div class="clear">&nbsp;</div>
                        <div id="addon-more" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
                                <h2 id="myModalLabel">Add-On Development</h2>
                            </div>
                            <div class="modal-body">
                                <p>Sometimes you just need a little added functionality in your site.</p>
                                <p>With our experience with many of the CMS systems available, we are more than capable of helping you plan, build and launch your custom add-on to your site.</p>
                                <p>We are familiar with CMS systems such as Wordpress, Drupal, Magneto. As well as frameworks such as Symfony2 and Zend Framework.  If your site is a custom build, we can still take a look at it and add the functionality that you want.</p>
                            </div>
                            <div class="modal-footer title2">
                                <h2 class="pull-left"><a class="" href="<?php echo $base; ?>contact">Contact Us Today</a></h2>
                                <button class="btn" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="box2">
                    <div class="span1 hidden-phone">
                        <img src="img/marker_1.gif">
                    </div>
                    <div class="span11">
                        <a name="design"></a>
                        <h2>Graphic Design</h2>
                        <p>First impressions are everything and we can help make it a smart one. Our designers are here to help you design your logo, website, mobile design, print media and more.</p>
                        <div class="margin-bottom center">
                            <a href="#" class="button" data-toggle="modal" data-target="#design-more"><span><span>Read More</span></span></a>
                        </div>
                        <div class="clear">&nbsp;</div>
                        <div id="design-more" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
                                <h2 id="myModalLabel">Graphic Design</h2>
                            </div>
                            <div class="modal-body">
                                <p>It's more than just looks. Before we start any design work, we need to understand the company behind the name. We need to know the company's mission, goals, target market, competitive advantages, competitors and preferences to establish valuable leverage. By working closely with our clients, we are able to come up with smart design tactics that contribute to a more effective design.</p>
                                <p>To stand out online, a functional, detail oriented and user friendly design is essential. Whether it's a fresh design or a redesign, we will strive to make your designs stand out. In addition to website design, we offer logo and banner designs, layout designs, various ad designs, landing page designs, and more.</p>
                                <p>To further enhance your great design, please see our Web Development Service to turn your site into a unique interactive experience for your customers.</p>
                                <ul class="nav nav-tabs no-margin-bottom">
                                    <li class="active"><a href="#design-website">Website Design</a></li>
                                    <li class=""><a href="#design-logo">Logo Design</a></li>
                                    <li class=""><a href="#design-other">Other Designs</a></li>
                                </ul>
                                <div class="tab-content box2 no-margin-top">
                                    <div class="tab-pane active" id="design-website">
                                        <p>J&amp;L Core Systems has over 10 years of experience in designing websites for individuals to small companies to large corporations. In our time in web design, we have seen it all. We started in the times where pages were static, heavy on images, and not at all user friendly. Now, things have changed, websites are designed to be elegant, simple yet eye-catching and engaging for the users.  We have watched and been part of the ever changing online trends and strive to stay current to what todays users expect and want to see.</p>
                                        <p>We are dedicated to creating effective, one of a kind website designs that are consistent with our client's needs, yet maintain a balance to what customers/users want to see. Our professional team will deliver a dynamic, clean, sophisticated user-friendly design that is results-driven.</p>
                                        <p>In today's online market, with desktops, tablets and smart phones, you will want to ensure that your designs will be viewable by anyone anywhere. We create all our designs to be responsive to the devices which will be viewing our website designs.</p>
                                        <p class="text-center">
                                        <strong>See for yourself! Change the width of our website and see how the site changes to display on smaller or larger devices.</strong>
                                        </p>
                                    </div>
                                    <div class="tab-pane" id="design-logo">
                                        <p>A strong corporate identity can sometimes be defined by a company logo alone. After all, logos appear on almost all communications. The right combination of letters, symbols and colors can resonate with a company's character and the principles it stands for.</p>
                                        <p>Let us help you design your logo that stands out and sets you apart from the competition.</p>
                                        <p><strong>Initial consultation</strong> : gather information pertaining to the company, what they do, typical customers, competition, existing design styles...etc.</p>
                                        <p><strong>Research</strong> : research the competition and marketplace for the company and design trends</p>
                                        <p><strong>Design concepts</strong> : create a variety of mock-up designs for the company</p>
                                        <p><strong>Feedback and Review</strong> : review mock-up designs with the company, get feedback, design changes and either decide on a design or go back to creating more mock-ups</p>
                                        <p><strong>Presentation</strong> : Present the final, complete, polished logo.</p>
                                    </div>
                                    <div class="tab-pane" id="design-other">
                                        <p>We offer other forms of design services as well.  We can offer designs for <strong>banners</strong>, <strong>landing pages</strong>, <strong>print media</strong>, and any other form of design work you may need.</p>
                                        <p>Let us help you design what you need to compliment your company and to target your customers.</p>
                                        <p>Do not hesitate to contact us with your needs.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer title2">
                                <h2 class="pull-left"><a class="" href="<?php echo $base; ?>contact">Contact Us Today</a></h2>
                                <button class="btn" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="box2">
                    <div class="span1 hidden-phone">
                        <img src="img/marker_1.gif">
                    </div>
                    <div class="span11">
                        <a name="web_development"></a>
                        <h2>Web Development</h2>
                        <p>Every business needs a website, and the professional look and feel of your site will directly impact the perception of your business to potential customers. With more and more customers reviewing a business's website prior to contacting them, what do you want your first impression to be?</p>
                        <p>Web development is not simply <strong style="text-decoration: line-through">web design</strong>. It is much much more. Designing a website is a completely different skill from developing a website.  We can offer the full package when it comes to developing your website.</p>
                        <p>The design phase is primary focused on the information provided by the client to design the overall look and feel of the desired website. The web development process takes things a bit further by adding value to the design. Web developers will turn your static website into a dynamic environment that will engage the customer.</p>
                        <blockquote>
                            Modern websites are far more than simple HTML as web programming (web development) has become increasingly sophisticated. Design principles will only get you so far. You need a strong team of technical website programmings to ensure that your website functions correctly on the complex and ever-changing web.
                        </blockquote>
                        <div class="margin-bottom center">
                            <a href="#" class="button" data-toggle="modal" data-target="#development-more"><span><span>Read More</span></span></a>
                        </div>
                        <div class="clear">&nbsp;</div>
                        <div id="development-more" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
                                <h2 id="myModalLabel">Web Development</h2>
                            </div>
                            <div class="modal-body title2">
                                <p>Our web development solutions will provide you with endless possibilities for enhancing your online presence. From self-managed websites to custom database applications, we will tailor our services to fit your needs. All our web development services include Project management, Design services, and the actual Web development (coding) services.</p>
                                <p>Using the latest technologies in PHP web development, we can create your custom web application for you. Our strategies include e-Commerce solutions, automated billing, appointment calendars, media galleries, online subscriptions, and more.</p>
                                <h2>Content Management Systems (CMS)</h2>
                                <p>A content management system (CMS) is a web based software platform which allows you to manage and update content within your website. It lets everyday users and business owners to control their sites without technical training, making it perfect for sites which need frequent updating.</p>
                                <p>Choosing the right CMS for your business will help you succeed in a flexible and affordable manner. Let us help you decide which would best suit your needs. Wordpress, Drupal, Magneto ...and others.</p>
                                <h2>Custom Web Development</h2>
                                <div class="text-center"><h1>If you can dream it, we can build it!</h1></div>
                                <p>If a pre-built CMS solution is not what you are looking for, we can certainly develop a custom CMS for you. A custom CMS built from the ground up, taking on a custom approach to suit your exact needs. Our custom builds are based on criteria including security, support, performance, flexibility, and ease of use.</p>
                            </div>
                            <div class="modal-footer title2">
                                <h2 class="pull-left"><a class="" href="<?php echo $base; ?>contact">Contact Us Today</a></h2>
                                <button class="btn" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            </article>
            <article class="span4">
                <div class="padding">
                <h1>Proficiencies</h1>
                <ul class="list1">
                    <li class="underline"><h2 class="small">Linux</h2></li>
                    <li>Windows</li>
                    <li class="underline"><h2 class="small">Apache</h2></li>
                    <li class="underline"><h2 class="small">MySQL</h2></li>
                    <li>PostgreSql</li>
                    <li class="underline"><h2 class="small">PHP</h2></li>
                    <li>Ajax</li>
                    <li>Javascript</li>
                    <li class="underline"><h2 class="small">jQuery</h2></li>
                    <li>ExtJs</li>
                    <li>MVC Architecture</li>
                    <li>OOP (Object oriented programming) </li>
                    <li>XML</li>
                    <li>XHTML</li>
                    <li class="underline"><h2 class="small">HTML5</h2></li>
                    <li class="underline"><h2 class="small">CSS3</h2></li>
                    <li class="underline"><h2 class="small">Photoshop</h2></li>
                    <li>Illustrator</li>
                    <li>Responsive Designs</li>
                    <li>Agile Development</li>
                    <li class="underline"><h2 class="small">PHP Consultation</h2></li>
                    <li class="underline"><h2 class="small">Project Management</h2></li>
                    <li>Current coding standards</li>
                    <li>W3C Standards and browser compatibility</li>
                    <li>Problem Solving</li>
                    <li>Results Oriented</li>
                    <li>Attention to Details</li>
                    <li class="underline"><h2 class="small">SYMFONY2</h2></li>
                    <li class="underline"><h2 class="small">Doctrine2</h2></li>
                    <li>Wordpress</li>
                    <li>Drupal</li>
                    <li>Zend Framework</li>
                    <li>Joomla</li>
                    <li>PHPNuke</li>
                    <li>Mobile Development</li>
                    <li>REST/SOAP</li>
                    <li>Amazon Web Services (AWS)</li>
                    <li>Facebook API</li>
                    <li>CMS(Content Management Systems)</li>
                    <li>SVN</li>
                    <li class="underline"><h2 class="small">GIT</h2></li>
                </ul>
                </div>
                <div class="clear"></div>
                <div class="title2 text-center">
                    <h2>We LOVE to code.</h2>
                    <h2>We DREAM in code.</h2>
                </div>
                <div class="clear"></div>
            </article>
        </section>
    </div>
<?php $contents = ob_get_clean(); ?>
<?php include( 'header.php' ); ?>
<?php include( 'half_header.php' ); ?>
<?php echo $contents; ?>
<?php include( 'footer.php' ); ?>