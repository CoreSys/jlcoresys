<?php $base = strstr( $_SERVER[ 'HTTP_HOST' ], 'localhost' ) || strstr( $_SERVER[ 'HTTP_HOST' ], '127.0.0.1' ) ? '/development/portfolio/' : '/'; ?>
<?php ob_start(); ?>
<?php $head_js_files = array( ); ?>
<?php $body_js_files = array( 'bootstrap-modal','bootstrap-tab','services' ); ?>
<?php $page = 'about'; ?>
<?php $keywords = 'about,about us,team'; ?>
<?php $title = 'Services'; ?>
    <div class="content-wrapper">
        <section class="row about box2 even">
        <h1 style="padding:0;margin:0;">J&amp;L Core Systems</h1>
        <p>We are proud to be a Victoria, BC based web development company.  We strive to deliver the best possible options with the best service to our clients.</p>
        <p>Unofficially, we have been developing websites and associated graphic designs since 1998, and in 2008 came together to form J&amp;L Core Systems.</p>
        <p>We have built more than a capable team, but a team that loves what they do, and who better have work on your project than a group that loves doing what they do? You will gain the benefits of us always staying current and up to date with the latest trends, delivering the absolute best coding and graphics ensuring that you get the most out of your experience with us.</p>
        <p>Since we have started, we have worked on all types of sites from personal to small business to large government branches. Every web project is unique, requires a unique aproach and requires a team that wants to take the time to get it right.</p>
        <p>Do you have a project that requires a lot of problem solving? or you have been told that it's too difficult? or maybe even told impossible? These are our favorite projects to take on. Over the years, we have successfully started and completed several projects which were deemed impossible by other web development companies.</p>
        <a href="<?php echo $base; ?>contact">Contact us today to tell us what you need.</a>
        </section>
        <section class="row about box2 odd">
            <img src="img/josh.jpg?t=<?php echo time(); ?>" />
            <h1>Josh McCreight</h1>
            <h2>President/Lead Developer</h2>
            <p>Josh started learning PHP programming early 1998 and has been hooked on it ever since. To complement the PHP programming he underwent a diploma program in 2000-2001 for web/graphic design. In 2003, he co-founded another local web development company focusing on online business development.  To further enhance his abilities, in 2008 he attended the University of Victoria and obtained his degree in computer science with an emphasis on gaming and graphics.</p>
            <p>Josh is proficient in PHP, HTML, HTML5, XHTML, CSS3, MySQl/PostgreSQL, SQL, Javascript, jQuery, ExtJs, Zend Framework, Symfony1.x, Symfony2.x, Doctrine, MVC Architecture, OOP, Photoshop, Illustrator, SVN, GIT, and much more.</p>
            <blockquote>Ever hear programmers who are passionate about their code say that they sometimes dream in code? Well, I am one of those. Not  a night goes by without dreaming in code.</blockquote>

        </section>
        <section class="row about box2 even">
            <img src="img/brian.jpg?t=<?php echo time(); ?>" />
            <h1>Brian Simmons</h1>
            <h2>Project Manager</h2>
            <p>Still awaiting a great write-up about Brian Simmons.</p>
        </section>
        <section class="row about box2 odd">
            <img src="img/adam.jpg?t=<?php echo time(); ?>" />
            <h1>Adam Withers</h1>
            <h2>Developer</h2>
            <p>Adam started programming in AppleScript / Hypercard in 1994 (creating his own version of Mortal Kombat), and HTML development in 1995 (creating a Spice Girls fan-page).  Graduating from Camosun College shortly after highschool, he completed 18 months of co-op work terms.  While creating a Google Analytics-esque application, he fell head over heels for PHP's simplicity and developer culture.  Adam's career has seen him thrown into many different Web Programming Environments including Onyx, Adobe Flex/Flash, and Apostrophe.</p>
            <p>Adam is specialized in systems Linux, Apache, MySQL, Javascript, jQuery, ExtJs, Zend Framework, MVC Architecture, various version-control methods.  But revels in the chance to be thrown into new technologies, and adapt his wide variety of experience to them.</p>
        </section>
        <section class="row about box2 even">
            <img src="img/jennifer.jpg?t=<?php echo time(); ?>" />
            <h1>Jennifer McNaughton</h1>
            <h2>Designer</h2>
            <p>Jennifer McNaughton has a flair for art. She has always had a thing for art, doodling on everything she could get her hands on. So, it was only natural that she pushed further into the art world.</p>
            <p>She has done several years of intensive courses in art and design at Camosun College. Has taken on many art projects in multiple mediums locally and <strong>worldwide</strong>.  With the world the way it is and digital graphics being a leading form of design, she went back to Camosun College to learn more about web and graphic design.</p>
            <p>Jennifer brings the flare of a creative personality and design to any project she undertakes. She has a unique view and will deliver more that then the ordinary. With a traditional art background coupled with digital graphic design, you are sure to get something that will stand out from the rest.</p>
        </section>
    </div>
<?php $contents = ob_get_clean(); ?>
<?php include( 'header.php' ); ?>
<?php include( 'half_header.php' ); ?>
<?php echo $contents; ?>
<?php include( 'footer.php' ); ?>