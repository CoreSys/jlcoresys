<div class="header-wrapper">
    <div class="header">
        <?php include( 'header_nav.php' ); ?>
        <?php include( 'nivo-slider1.php' ); ?>
        <div class="bottom-wrapper hidden-phone">
            <div class="bottom">
                <div class="row-fluid">
                    <div class="span2 large-padding-top text-center hidden-tablet"><img src="img/header-bottom-left.png" /></div>
                    <div class="span8 text-center text-capitalize title2">
                        <h2>We Love what we do and <span>it shows</span></h2>
                        <h1>Do what you love <span>&</span> Success will follow</h1>
                    </div>
                    <div class="span2 large-padding-top text-center hidden-tablet"><img src="img/header-bottom-right.png" /></div>
                </div>
            </div>
        </div>
    </div>
    <div class="greyed text-center">
        <h2 class="small">HTML5 - CSS3 - PHP - MySQL - jQuery - Project Management - Consulting - Since 1998</h2>
        <h2 class="small">Victoria BC</h2>
        <div class="title2" style="color:#000;">
        <h2>If you have a difficult project or have been told it cannot be done, we want to hear from you!</h2>
        </div>
    </div>
</div>