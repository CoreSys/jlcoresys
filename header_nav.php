<div class="top">
    <div class="header-nav-wrapper">
        <ul class="header-nav">
            <li class="brand"><img class="hidden-phone hidden-tablet" src="img/header-nav-logo.jpg" /><a class="full" href="<?php echo $base; ?>"></a></li>
            <li class="item item-home<?php echo $page == 'home' ? ' active' : ''; ?>"><a class="full" href="<?php echo $base; ?>"></a></li>
            <li class="item item-services<?php echo $page == 'services' ? ' active' : ''; ?>"><a class="full" href="<?php echo $base; ?>services"></a></li>
            <li class="item item-about<?php echo $page == 'about' ? ' active' : ''; ?>"><a class="full" href="<?php echo $base; ?>about"></a></li>
            <li class="item item-portfolio<?php echo $page == 'portfolio' ? ' active' : ''; ?>"><a class="full" href="<?php echo $base; ?>portfolio"></a></li>
            <li class="item item-contact<?php echo $page == 'contact' ? ' active' : ''; ?>"><a class="full" href="<?php echo $base; ?>contact"></a></li>
            <li class="brand">
                <img class="hidden-phone hidden-tablet" src="img/header-nav-search2.gif" />
<!--                <form class="hidden-phone hidden-tablet search-form" action="javascript:void(0);" id="search-form" method="post">-->
<!--                    <input class="search" type="text" id="search-words" name="search" value="Search" />-->
<!--                    <div class="submit" onclick="$('#search-form').submit();"></div>-->
<!--                </form>-->
            </li>
        </ul>
    </div>
</div>