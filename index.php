<?php $base = strstr( $_SERVER[ 'HTTP_HOST' ], 'localhost' ) || strstr( $_SERVER[ 'HTTP_HOST' ], '127.0.0.1' ) ? '/development/portfolio/' : '/'; ?>
<?php ob_start(); ?>
    <?php $head_js_files = array( ); ?>
    <?php $body_js_files = array( ); ?>
    <?php $page = 'home'; ?>
    <div class="content-wrapper">
        <section class="row-fluid">
            <article class="span4">
                <div class="box1">
                    <div class="box1-bot">
                        <div class="box1-top">
                            <div class="small-padding-left small-padding-right">
                                <div class="">
                                    <div class="text-center">
                                        <h2 class="center">Expert PHP Consulting</h2>
                                    </div>
                                    <p>You can't always do it yourself. Sometimes you need a little help. Let us give you a hand. We have worked on all kinds of systems and projects and have seen almost all there is to be seen. With over a decade of experience, we will help you achieve your goals.</p>
                                    <div class="box1-btn margin-bottom center">
                                        <a href="<?php echo $base; ?>services#consulting" class="button"><span><span>More</span></span></a>
                                    </div>
                                    <div class="clear">&nbsp;</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
            <article class="span4">
                <div class="box1">
                    <div class="box1-bot">
                        <div class="box1-top">
                            <div class="small-padding-left small-padding-right">
                                <div class="">
                                    <div class="text-center">
                                        <h2 class="center">Project Management</h2>
                                    </div>
                                    <p>
                                        Have an idea and need help with getting it going? Let us help. We can help you to plan out your project from start to finish and help with all the details along the way. We have experience with all types of projects large and small. So don't hesitate to ask us for assistance.
                                    </p>
                                    <div class="box1-btn margin-bottom center">
                                        <a href="<?php echo $base; ?>services#project_management" class="button"><span><span>More</span></span></a>
                                    </div>
                                    <div class="clear">&nbsp;</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
            <article class="span4">
                <div class="box1">
                    <div class="box1-bot">
                        <div class="box1-top">
                            <div class="small-padding-left small-padding-right">
                                <div class="">
                                    <div class="text-center">
                                        <h2 class="center">Custom Applications</h2>
                                    </div>
                                    <p>
                                        Do you have a unique idea that you would like to see developed? Let us help you plan, build, test and deploy your idea. Use our experience in project management, design and development to turn your ideas into reality.
                                    </p>
                                    <div class="box1-btn margin-bottom center">
                                        <a href="<?php echo $base; ?>services#custom_applications" class="button"><span><span>More</span></span></a>
                                    </div>
                                    <div class="clear">&nbsp;</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </section>
        <section class="row-fluid">
            <article class="span4">
                <div class="box1">
                    <div class="box1-bot">
                        <div class="box1-top">
                            <div class="small-padding-left small-padding-right">
                                <div class="">
                                    <div class="text-center">
                                        <h2 class="center">Add-On Development</h2>
                                    </div>
                                    <p>
                                        Already have an existing website? Does it use a current PHP framework such as Wordpress, Symfony, Zend Framework, Magneto, Drupal, or another? If you need more functionality or features for your site, let us help. We have over a decade of experience with PHP frameworks and know how they work and how to add the functionality that you want.
                                    </p>
                                    <div class="box1-btn margin-bottom center">
                                        <a href="<?php echo $base; ?>services#addons" class="button"><span><span>More</span></span></a>
                                    </div>
                                    <div class="clear">&nbsp;</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
            <article class="span4">
                <div class="box1">
                    <div class="box1-bot">
                        <div class="box1-top">
                            <div class="small-padding-left small-padding-right">
                                <div class="">
                                    <div class="text-center">
                                        <h2 class="center">Graphic Design</h2>
                                    </div>
                                    <p>
                                        Just need some design work done? Not a problem. We have experience with logo design, site layout designs, print media and more. Using photoshop or another mainstream application we can create your graphics for you quickly and to your specifications.
                                    </p>
                                    <div class="box1-btn margin-bottom center">
                                        <a href="<?php echo $base; ?>services#design" class="button"><span><span>More</span></span></a>
                                    </div>
                                    <div class="clear">&nbsp;</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
            <article class="span4">
                <div class="box1">
                    <div class="box1-bot">
                        <div class="box1-top">
                            <div class="small-padding-left small-padding-right">
                                <div class="">
                                    <div class="text-center">
                                        <h2 class="center">Web Development</h2>
                                    </div>
                                    <p>Need the complete solution? Put us to work on your project. We will help you from start to finish. We will plan, design, test, and launch your project. Using the latest web technologoes such as PHP, MySQL/PostgreSql, jQuery/ExtJs, HTML5 and more.</p>
                                    <div class="box1-btn margin-bottom center">
                                        <a href="<?php echo $base; ?>services#web_development" class="button"><span><span>More</span></span></a>
                                    </div>
                                    <div class="clear">&nbsp;</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </section>
    </div>
<?php $contents = ob_get_clean(); ?>
<?php include( 'header.php' ); ?>
<?php include( 'full_header.php' ); ?>
<?php echo $contents; ?>
<?php include( 'footer.php' ); ?>