<?php
if( isset( $body_js_files ) )
{
    $body_js_files[] = 'jquery.nivo.slider.pack';
    $body_js_files[] = 'slider';
} else {
    $body_js_files = array( 'jquery.nivo.slider.pack', 'slider' );
}
?>
<div class="middle-wrapper hidden-phone">
    <div class="middle">
        <div id="slider" class="slider nivoSlider">
            <img src="" data-src="img/slide1.jpg" alt="" />
            <img src="" data-src="img/slide2.jpg" alt="" style="display:none"/>
            <img src="" data-src="img/slide3.jpg" alt="" style="display:none"/>
        </div>
    </div>
</div>