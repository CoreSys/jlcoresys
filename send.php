<?php $base = strstr( $_SERVER[ 'HTTP_HOST' ], 'localhost' ) || strstr( $_SERVER[ 'HTTP_HOST' ], '127.0.0.1' ) ? '/development/portfolio/' : '/'; ?>
<?php
$redirect = null;
$link = null;
if( isset( $_POST ) && count( $_POST ) > 0 ) {
    try {
        // we have a post
        $vars = array( 'time', 'name', 'email', 'phone', 'type', 'message' );
        foreach( $vars as $var )
        {
            $$var = isset( $_POST[ $var ] ) ? $_POST[ $var ] : null;
        }

        $from = isset( $email ) ? $email : 'unknown@domain.com';
        $to = 'admin@jlcoresystems.com';
        $subject = 'New Contact Message From JLCoreSystems';

        $msg = 'From ' . $name . ' ' . $email . "\n";
        $msg .= 'Phone ' . $phone . "\n";
        $msg .= 'Contact Type ' . $type . "\n";
        $msg .= 'IP: ' . $_SERVER[ 'REMOTE_ADDR' ] . "\n";
        $msg .= $message;

        $res = mail( $to, $subject, $msg );

        echo '<script type="text/javascript">location.reload(true);</script>';
    } catch( \Exeption $e ) {

    }
}
?>
<?php ob_start(); ?>
<?php $head_js_files = array( ); ?>
<?php $body_js_files = array( 'bootstrap-modal','bootstrap-tab' ); ?>
<?php $page = 'contact'; ?>
<?php $keywords = 'contact,inquiry,inquire'; ?>
<?php $title = 'Contact Us'; ?>
    <div class="content-wrapper">
        <section class="row-fluid">
            <article class="span8">
                <div class="padding">
                    <h1>Youre Message Has Been Sent</h1>
                    <p class="padding text-center">
                        Please allow us a little time to recieve your message.
                        <br><br>
                        We will address your message as soon as we can.
                        <br><br>
                        Thank You
                    </p>
                    <div class="clear"></div>
                </div>
            </article>
            <article class="span4">
                <div class="padding">
                    <h1>Services</h1>
                    <ul class="list1">
                        <li><a href="<?php echo $base; ?>services#consulting">Expert PHP Consulting</a></li>
                        <li><a href="<?php echo $base; ?>services#project_management">Project Management</a></li>
                        <li><a href="<?php echo $base; ?>services#custom_applications">Custom Applications</a></li>
                        <li><a href="<?php echo $base; ?>services#addons">Add-On Development</a></li>
                        <li><a href="<?php echo $base; ?>services#design">Graphic Design</a></li>
                        <li><a href="<?php echo $base; ?>services#web_development">Web Development</a></li>
                    </ul>
                </div>
                <div class="clear"></div>
                <div class="title2 text-center">
                    <h2>We LOVE to code.</h2>
                    <h2>We DREAM in code.</h2>
                </div>
                <div class="clear"></div>
            </article>
        </section>
    </div>
<?php $contents = ob_get_clean(); ?>
<?php include( 'header.php' ); ?>
<?php include( 'half_header.php' ); ?>
<?php echo $contents; ?>
<?php include( 'footer.php' ); ?>