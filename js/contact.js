;$(document).ready(function(){
    $('.form').submit(function(e){
        var valid = true;
        if($('#inputMessage').val() == '') {
            alert( 'Please enter a message' );
            valid = false;
        }

        if(valid)
        {
            $.ajax({
                url: 'current_time.php',
                success: function(res){
                    var input = $('<input type="hidden" name="time" value="' + res + '"/>' );
                    $('.form').append(input);
                }
            });
        } else {
            e.preventDefault();
        }
    });
});