;
$(window).load(function () {
    $('[data-src]').each(function () {
        var src = $(this).attr('data-src');
        $(this).attr('src', src);
    });
    Cufon.now();
    $('input.search').focus(function () {
        if ($(this).val() == 'Search') {
            $(this).val('');
        }
        $(this).css('color', '#000');
    });
    var max_height = 0;
    $('.box1').each(function () {
        var h = $(this).height();
        if (h >= max_height) {
            max_height = h;
        }
    });
    $('.box1').each(function () {
        $(this).css('height', max_height + 'px');
        $(this).find('.box1-bot').css('height', '100%');
    });
    Cufon.replace('#menu a', { fontFamily: 'Copse', hover: true, color: '-linear-gradient(#fdfdfd, #dedede, #b4b4b4)' });
    Cufon.replace('h2, .button, .list2 a, #sign_up a, h3', { fontFamily: 'Copse', hover: true });
    Cufon.replace('h1', { fontFamily: 'Copse', hover: true });
});