$(document).ready(function(){
    setTimeout(function(){
        $('.samples .element').each(function(){
            var img = $(this).find('img');
            var w = img.width();
            var h = img.height();
            img.css({'width':w+'px','height':h+'px'});
            $(this).css('height',h+'px');
        });
    },250);
    setTimeout(function(){loadGallery()},500);
    $('.fancybox').fancybox();
});

function loadGallery()
{
    $('#tiles').imagesLoaded(function() {
        // Prepare layout options.
        var options = {
            itemWidth: 150, // Optional min width of a grid item
            autoResize: true, // This will auto-update the layout when the browser window is resized.
            container: $('#tiles'), // Optional, used for some extra CSS styling
            offset: 10, // Optional, the distance between grid items
            outerOffset: 20, // Optional the distance from grid to parent
            flexibleWidth: 300 // Optional, the maximum width of a grid item
        };

        // Get a reference to your grid items.
        var handler = $('#tiles li');

        // Call the layout function.
        handler.wookmark(options);
    });
}