<?php $base = strstr( $_SERVER[ 'HTTP_HOST' ], 'localhost' ) || strstr( $_SERVER[ 'HTTP_HOST' ], '127.0.0.1' ) ? '/development/portfolio/' : '/'; ?>
<?php ob_start(); ?>
<?php $head_js_files = array( ); ?>
<?php $body_js_files = array( 'bootstrap-modal','bootstrap-tab','contact' ); ?>
<?php $page = 'contact'; ?>
<?php $keywords = 'contact,inquiry,inquire'; ?>
<?php $title = 'Contact Us'; ?>
    <div class="content-wrapper">
        <section class="row-fluid">
    <article class="span8">
        <div class="padding">
            <h1>Contact Us</h1>
            <form class="form-horizontal form box2 padding" method="post" action="<?php echo $base . 'send'; ?>">
                <div class="control-group">
                    <label class="control-label required" for="inputName">Your Name</label>
                    <div class="controls">
                        <input type="text" name="name" id="inputName" placeholder="Name" required="required">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label required" for="inputEmail">Email</label>
                    <div class="controls">
                        <input type="email" name="email" id="inputEmail" placeholder="Email" required="required">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="inputPhone">Phone Number</label>
                    <div class="controls">
                        <input type="text" id="inputPhone" placeholder="Phone Number">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="inputType">Contact Type</label>
                    <div class="controls">
                        <select id="intputType" name="type">
                            <option value="general">General</option>
                            <option value="support">Support</option>
                            <option value="quote">Request a Quote</option>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label required" for="inputMessage">Message</label>
                    <div class="controls">
                        <textarea id="inputMessage" name="message" rows="10"></textarea>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls pull-right">
                        <input type="hidden" name="time" value="<?php echo time(); ?>" />
                        <button type="submit" class="btn">Send Message</button>
                    </div>
                </div>
            </form>
            <div class="clear"></div>
        </div>
    </article>
    <article class="span4">
        <div class="padding">
            <h1>Services</h1>
            <ul class="list1">
                <li><a href="<?php echo $base; ?>services#consulting">Expert PHP Consulting</a></li>
                <li><a href="<?php echo $base; ?>services#project_management">Project Management</a></li>
                <li><a href="<?php echo $base; ?>services#custom_applications">Custom Applications</a></li>
                <li><a href="<?php echo $base; ?>services#addons">Add-On Development</a></li>
                <li><a href="<?php echo $base; ?>services#design">Graphic Design</a></li>
                <li><a href="<?php echo $base; ?>services#web_development">Web Development</a></li>
            </ul>
        </div>
        <div class="clear"></div>
        <div class="title2 text-center">
            <h2>We LOVE to code.</h2>
            <h2>We DREAM in code.</h2>
        </div>
        <div class="clear"></div>
    </article>
    </section>
    </div>
<?php $contents = ob_get_clean(); ?>
<?php include( 'header.php' ); ?>
<?php include( 'half_header.php' ); ?>
<?php echo $contents; ?>
<?php include( 'footer.php' ); ?>