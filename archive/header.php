<?php $page = isset( $page ) ? $page : 'home'; ?>
<?php $title = isset( $title ) ? $title : null; ?>
<?php $keywords = isset( $keywords ) ? $keywords : null; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>J&L Core Systems <?php echo !empty( $title ) ? ' - ' . $title : ''; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="J&L Core Systems, experienced web design and development to suit all your needs. No job too hard, too small or too large.">
    <meta name="keywords" content="J&L Core Systems,symfony,zend,zend frameword,html5,css,js,javascript,jquery,extjs,wordpress,photoshop,victoria,bc,canada,design">
    <meta name="author" content="J&L Core Systems">

    <!-- Le styles -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/css/bootstrap-responsive.css" rel="stylesheet">
    <link href="assets/js/google-code-prettify/prettify.css" rel="stylesheet">
    <link href="assets/css/site.php" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="assets/js/html5shiv.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="assets/ico/favicon.png">
</head>
<body data-spy="scroll">
<!--[if lt IE 7]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
<![endif]-->
<div class="container large-margin-top">
    <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <a class="brand" href="index">J&amp;L Core Systems</a>
                <div class="nav-collapse collapse pull-right pull-left-in-collapse">
                    <ul class="nav">
                        <li class="<?php echo $page == 'home' ? 'active ' : ''; ?>li-navbar"><a href="index"><i class="icon-home"></i> Home</a></li>
                        <li class="<?php echo $page == 'services' ? 'active ' : ''; ?>li-navbar"><a href="services"><i class="icon-certificate"></i> Services</a></li>
                        <li class="<?php echo $page == 'portfolio' ? 'active ' : ''; ?>li-navbar"><a href="portfolio"><i class="icon-tasks"></i> Portfolio</a></li>
                        <li class="<?php echo $page == 'contact' ? 'active ' : ''; ?>li-navbar"><a href="contact"><i class="icon-envelope"></i> Contact</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="img-polaroid margin-top">
            <div id="myCarousel" class="carousel slide">
                <div class="carousel-inner">
                    <div class="item active">
                        <img src="assets/img/car1.jpg" alt="J&L Core Systems" />
                        <div class="carousel-caption">
                            <h1>J&amp;L Core Systems</h1>
                            <p class="lead">We at J&amp;L Core Systems believe in the best, highest quality.</p>
                        </div>
                    </div>
                    <div class="item">
                        <img src="assets/img/car2.jpg" alt="Clean and Simple" />
                        <div class="carousel-caption">
                            <h1>Clean and Simple</h1>
                            <p>
                                Long gone are the days of sites riddled with animated gifs and an ambundance of flash embedded objects.
                                <br>
                                Today, users like to see clean, usable and friendly websites.
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <img src="assets/img/car3.jpg" alt="Elegant Design" />
                        <div class="carousel-caption">
                            <h1>Elegant Designs</h1>
                            <p>
                                We strive in building elegantly designed websites that look, feel and function the way you want it to.
                                <br />
                                Let us help you design your next site the way you want it to be.
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <img src="assets/img/car4.jpg" alt="Functionality" />
                        <div class="carousel-caption">
                            <h1>Functionality</h1>
                            <p>
                                We pride ourselves in offering proper functionality in the sites we deliver.
                                <br>
                                Do you want more control over your website?
                                <br>
                                Do you want functionality that you currently do not have?
                                <br>
                                Contact us today, and let's create that functionality you want.
                            </p>
                        </div>
                    </div>
                </div>
                <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
                <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
            </div>
        </div>
        <hr />