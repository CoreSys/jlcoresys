<?php $page = 'home'; ?>
<?php include( 'header.php' ); ?>
    <div class="marketing no-margin-top">
        <!-- Three columns of text below the carousel -->
        <div class="row">
            <div class="span4">
                <img class="img-circle" data-src="holder.js/140x140">
                <h2>Simple</h2>
                <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</p>
                <p><a class="btn" href="#">View details &raquo;</a></p>
            </div><!-- /.span4 -->
            <div class="span4">
                <img class="img-circle" data-src="holder.js/140x140">
                <h2>Elagent</h2>
                <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
                <p><a class="btn" href="#">View details &raquo;</a></p>
            </div><!-- /.span4 -->
            <div class="span4">
                <img class="img-circle" data-src="holder.js/140x140">
                <h2>Sohpisticated</h2>
                <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
                <p><a class="btn" href="#">View details &raquo;</a></p>
            </div><!-- /.span4 -->
        </div><!-- /.row -->


        <!-- START THE FEATURETTES -->

        <hr class="featurette-divider">

        <div class="featurette">
            <img class="featurette-image pull-right" src="../assets/img/cons.png">
            <h2 class="featurette-heading">PHP Consulting. <span class="muted">Need some help?</span></h2>
            <p class="lead">
                Do you have a project in mind? That project that will be your claim to fame?<br>
                If you have your idea and don't know what to do with it, then you came to the right place. We can help you visualize your idea, help you plan it out, and help you along the way so that you can plan, build and deliver your idea to your target customers.
            </p>
        </div>

        <hr class="featurette-divider">

        <div class="featurette">
            <img class="featurette-image pull-left" src="../assets/img/desi.png">
            <h2 class="featurette-heading">Oh yeah, it's that good. <span class="muted">See for yourself.</span></h2>
            <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
        </div>

        <hr class="featurette-divider">

        <div class="featurette">
            <img class="featurette-image pull-right" src="../assets/img/examples/browser-icon-safari.png">
            <h2 class="featurette-heading">And lastly, this one. <span class="muted">Checkmate.</span></h2>
            <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
        </div>

        <hr class="featurette-divider">

        <!-- /END THE FEATURETTES -->


        <!-- FOOTER -->
        <footer>
            <p class="pull-right"><a href="#">Back to top</a></p>
            <p>&copy; 2013 Company, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p>
        </footer>

    </div><!-- /.container -->
<!--    <div class="row-fluid">-->
<!--        <div class="span4 align-center">-->
<!--            <h2 class="align-center">Simple</h2>-->
<!--            <p class="align-justify padding">-->
<!--                In todays world, the best websites are those that are simple use for both the site owners and site users. Clean designs that focus on your content engaging your users is our primary goal.-->
<!--            </p>-->
<!--        </div>-->
<!--        <div class="span4 align-center">-->
<!--            <h2 class="align-center">Design</h2>-->
<!--            <p class="align-justify">-->
<!--                Learn more on design-->
<!--            </p>-->
<!--        </div>-->
<!--        <div class="span4 align-center">-->
<!--            <h2 class="align-center">Development</h2>-->
<!--            <p class="align-justify">-->
<!--                Learn more on Development-->
<!--            </p>-->
<!--        </div>-->
<!--    </div>-->
<?php include( 'footer.php' ); ?>