<?php
header('Content-type: text/css');
echo 'body{background:#dedede url(../img/bg.png);}';
echo '.clear{clear:both;height:0px;}';
echo '.container{box-shadow:0px 0px 5px #ccc;background:rgba(255,255,255,.25);}';
echo '.center{margin:0 auto;}';
echo '.header{background:transparent url(../img/hbg1.jpg) no-repeat center center;}';
$sizes = array( '0' => '18px', 'small' => '6px', 'med' => '12px', 'large' => '24px' );
$types = array( 'margin', 'padding' );
$positions = array( '0', 'top', 'bottom', 'left', 'right' );
foreach( $types as $type )
{
    foreach( $sizes as $size => $px )
    {
        $size = $size != '0' ? $size : null;
        foreach( $positions as $pos )
        {
            $pos = $pos != '0' ? $pos : null;
            echo '.' . ( !empty( $size ) ? $size . '-' : '' ) . $type . ( !empty( $pos ) ? '-' . $pos : '' ) . '{' . $type . ( !empty( $pos ) ? '-' . $pos : '' ) . ':' . $px . ';}';
        }
    }
}

$aligns = array( 'center', 'left', 'right', 'justify' );
foreach( $aligns as $align )
{
    echo '.align-' . $align . '{text-align:' . $align . ';}';
}
echo '.navbar .nav{display:inline-block;float:none;}';
echo '.navbar .container {box-shadow:none;height:40px;}';
?>
.navbar-inverse,
.navbar-inverse .navbar-inner,
.navbar-inverse .navbar-inner .container { background: #1b1b1b; height:40px; }
.carousel-caption {background-color: rgba(0,0,0,.5);position: absolute;padding: 0 20px;margin-bottom:0px;padding:10px;width:100%;text-align:center;}
.carousel-caption h1 {font-size:3em;}
.carousel-caption h1,.carousel-caption .lead {margin: 0;line-height: 1.25;color: #fff;text-shadow: 0 1px 1px rgba(0,0,0,.4);}
.carousel-caption .btn {margin-top: 10px;}
.row.odd blockquote { color: #666; font-size: 11px; }