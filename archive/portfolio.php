<?php $page = 'portfolio'; ?>
<?php $title = 'Portfolio work'; ?>
<?php $keywords = 'portfolio,work,completed,work completed,clients'; ?>
<?php include( 'header.php' ); ?>
<div class="row-fluid">
    <div class="span4 align-center">
        <h2 class="align-center">Consulting</h2>
        <p class="align-justify">
            Learn more on consulting
        </p>
    </div>
    <div class="span4 align-center">
        <h2 class="align-center">Design</h2>
        <p class="align-justify">
            Learn more on design
        </p>
    </div>
    <div class="span4 align-center">
        <h2 class="align-center">Develpment</h2>
        <p class="align-justify">
            Learn more on development
        </p>
    </div>
</div>
</div>
</body>
</html>