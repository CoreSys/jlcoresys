<?php $page = 'services'; ?>
<?php $title = 'Services'; ?>
<?php $keywords = 'service,services,available services,what we do,what we can do,availability'; ?>
<?php include( 'header.php' ); ?>
<div class="row-fluid">
    <?php $types = array( 'Logo Design', 'Site Design', 'Site Planning', 'CMS Design', 'Consultation', 'Symfony2 Development', 'Zend Framework Development', 'Project Planning', 'SEO Services', 'Add-On Development' ); ?>
    <?php foreach( $types as $type ): ?>
    <div class="span4">
        <h4><?php echo $type; ?></h4>
        <p><?php echo $type; ?></p>
    </div>
    <?php endforeach; ?>
</div>
<?php include( 'footer.php' ); ?>