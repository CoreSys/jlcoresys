<?php include( 'header.php' ); ?>
<div class="row">
    <div class="span2">
        <img src="assets/img/consulting.png" />
    </div>
    <div class="span10">
        <div class="padding-right">
            <h1>Expert PHP Consulting</h1>
            <p>
                To most, talking about a programming language such as PHP sounds like tech talk and does not make much sense. There are many platforms, frameworks and CMS sites which use PHP, which include 'Wordpress', 'Drupal', 'Joomla', 'Zend Framework', 'Symfony', 'Magneto', and many more. When looking for a PHP programmer, you are looking for someone who has the skills necessary to modify or customize all of these.
                <br><br>
                If you are looking to hire a professional PHP developer for an important project we can help you. We have 10+ years experience working with PHP and the several open source frameworks/platforms which are built on PHP.
                <br><br>
                We can work together with you to understand your needs and come up with an appropriate feasible solution that works for you. We do understand that you may not be able to understand whats all involved. Thats OK, we will make things simple for you and explain details in such a way that you will understand.
                <br><br>
                <a class="btn btn-large btn-primary" href="#">Contact Us For a Free Consultation</a>
            </p>
        </div>
    </div>
</div>
<hr>
<div class="row odd">
    <div class="span10">
        <div class="padding-left">
            <h1>PHP Frameworks/Platforms</h1>
            <p>
                Do you know about the various PHP frameworks available? Do you have a project in mind that requires expertise in working with PHP frameworks? We can help you with your needs when it comes to working with PHP frameworks. We have worked with most of the major  PHP framewords/platforms and are profficient in adapting these to your needs.
                <br><br>
                If you are beginning a project and are unsure where to start, thats ok, we can help. We can discuss your project and see which PHP framework will work best for you.
                <br><br>
                Already have an existing framework and want to add to it? We can help here too. We can discuss your add-on needs and set up a plan to make this happen.
                <br><br>
                Using a PHP framework can help reduce the amount of work that is needed to get your site up and running. Be cautious though, you will still require someone that knows how to work with these frameworks.
                <br><br>
                <a class="pull-right btn btn-large btn-primary" href="#">Contact Us For a Free Consultation</a>
            <div class="clear"></div>
            </p>
        </div>
    </div>
    <div class="span2">
        <img src="assets/img/frameworks.png" />
    </div>
</div>
<hr>
<div class="row odd">
    <div class="span2">
        <img src="assets/img/planning.png" />
    </div>
    <div class="span10">
        <div class="padding-right">
            <h1>Project Planning</h1>
            <p>
                Believe it or not, this may seem simple enough to most, but this is the most overlooked step when it comes to any project. Many will scribble down a few notes and dive into development without proper planning. This may work for some, but most will find that without proper planning, the projects tend to take longer, use more resources, or most likely 'hit a wall'.
                <br><br>
                Once you have an idea for a project or a change to a project, we can help you come up with a project plan that suits your needs. We will help you dig deep into what the project is about and what all is needed to get it successfully up and running.
                <br><br>
                <a class="btn btn-large btn-primary" href="#">Contact Us For a Free Consultation</a>
            </p>
        </div>
    </div>
</div>
<hr>
<div class="row odd">
    <div class="span10">
        <div class="padding-left">
            <h1>Design/Development</h1>
            <p>
                A website design should reinforce your brand identity and guide your future marketing decisions, strategies and development. Prior to getting started you will need to undergo project planning to get a solid foundation. Our team will help you to design eye-catching, modern, simple designs for your site, logos, and print media.
                <br><br>
                We have been developing custom websites since 1998 for individuals, small companies to large corporations. Put our vast experience in design and development to use for you to create a visually appealing functional site. Our design and coding practices are optimized for your target audience.
                <br><br>
                We understand the importance of retaining your visitors. Our sites incorporate up-to-date technologies such as HTML5, CSS3.x, custom Javascript, jQuery, ExtJs, PHP, MySQL, PostgreSql, among other technologies. We implement cross browser compatibility allowing users on different browsers to properly view the site.
                <br><br>
                <a class="pull-right btn btn-large btn-primary" href="#">Contact Us For a Free Consultation</a>
            <div class="clear"></div>
            </p>
            <blockquote>
                Web page design is a process of conceptualization, planning, modeling, and execution of electronic media content delivery via Internet in the form of technologies (such as markup languages) suitable for interpretation and display by a web browser or other web-based graphical user interfaces (GUIs).
                <br><br>
                Improvements in the various browsers' compliance with W3C standards prompted a widespread acceptance of XHTML and XML in conjunction with Cascading Style Sheets (CSS) to position and manipulate web page elements. The latest standards and proposals aim at leading to the various browsers' ability to deliver a wide variety of media and accessibility options to the client possibly without employing plug-ins.
                <br><br>
                Typically web pages are classified as static or dynamic.
                <br><br>
                Static pages don’t change content and layout with every request unless a human (web master or programmer) manually updates the page.
                Dynamic pages adapt their content and/or appearance depending on the end-user’s input or interaction or changes in the computing environment (user, time, database modifications, etc.) Content can be changed on the client side (end-user's computer) by using client-side scripting languages (JavaScript, JScript, Actionscript, media players and PDF reader plug-ins, etc.) to alter DOM elements (DHTML). Dynamic content is often compiled on the server utilizing server-side scripting languages (PHP, ASP, Perl, Coldfusion, JSP, Python, etc.). Both approaches are usually used in complex applications.
                With growing specialization within communication design and information technology fields, there is a strong tendency to draw a clear line between web design specifically for web pages and web development for the overall logistics of all web-based services.
            </blockquote>
        </div>
    </div>
    <div class="span2">
        <img src="assets/img/design.png" />
    </div>
</div>
<hr>
<div class="row">
    <div class="span2">
        <img src="assets/img/php.png" />
    </div>
    <div class="span10">
        <div class="padding-right">
            <h1>Web Programming</h1>
            <p>
                Our language of preference is PHP! We strive to use current coding standards and practices. We heavily use OOP and MVC structures for our projects.
                Along with PHP, we prefer to implement our sites with MySql/PostgreSql, jQuery, HTML5, and Css3.x.
                <br><br>
                There are no projects too small, too large, too simple, or too complicated. Bring us your ideas and we can make something work. The harder you think your project is, the more we want to hear about it.
                <br><br>
                Need a custom site with all the bells and whistles and cannot find an appropriate open source CMS that will do the job? We can help you plan, design, build, and launch your site to meet your exact requirements.
                <br><br>
                <a class="btn btn-large btn-primary" href="#">Contact Us For a Free Consultation</a>
            </p>
        </div>
    </div>
</div>
<hr>
<div class="row odd">
    <div class="span10">
        <div class="padding-left">
            <h2>Blog, Content Management Systems &amp; Web Applications</h2>
            <p>
                Advanced web programming using server site languages and database management services. We are highly experienced in Content Management system design and blog development. Using CMS systems and Blogs are a powerful resource for creating a Search Engine Optimized Web 2.0 web site.
                <br><br>
                The concept of Web 2.0 is to retain web visitors and create a viral marketing campaign for your site.
                <br>
                Social Networking is on the forefront of the internet and provides the most powerful tool for increasing your web traffic.
                <br>
                The onset of advanced pay per click advertising models such as Text Link Advertising, Google Adsense and companies such as LinkXL have opened many doors for creating a self funding web project.
                <br><br>
                <a class="pull-right btn btn-large btn-primary" href="#">Contact Us For a Free Consultation</a>
            <div class="clear"></div>
            </p>
            <blockquote>
                Web 2.0 describes the changing trends in the use of World Wide Web technology and web design that aim to enhance creativity, communications, secure information sharing, collaboration and functionality of the web. Web 2.0 concepts have led to the development and evolution of web culture communities and hosted services, such as social-networking sites, video sharing sites, wikis, blogs, and folksonomies. The term became notable after the first O'Reilly Media Web 2.0 conference in 2004.[1][2] Although the term suggests a new version of the World Wide Web, it does not refer to an update to any technical specifications, but rather to changes in the ways software developers and end-users utilize the Web.
                <br>
                Web 2.0 is the business revolution in the computer industry caused by the move to the Internet as a platform, and an attempt to understand the rules for success on that new platform. A blog (a contraction of the term "Web log") is a Web site, usually maintained by an individual with regular entries of commentary, descriptions of events, or other material such as graphics or video. Entries are commonly displayed in reverse-chronological order. "Blog" can also be used as a verb, meaning to maintain or add content to a blog.
                <br>
                Many blogs provide commentary or news on a particular subject; others function as more personal online diaries. A typical blog combines text, images, and links to other blogs, Web pages, and other media related to its topic. The ability for readers to leave comments in an interactive format is an important part of many blogs. Most blogs are primarily textual, although some focus on art (artlog), photographs (photoblog), sketches (sketchblog), videos (vlog), music (MP3 blog), audio (podcasting), which are part of a wider network of social media. Micro-blogging is another type of blogging, one which consists of blogs with very short posts. As of December 2007, blog search engine Technorati was tracking more than 112 million blogs.[1] With the advent of video blogging, the word blog has taken on an even looser meaning — that of any bit of media wherein the subject expresses his opinion or simply talks about something.
            </blockquote>

        </div>
    </div>
    <div class="span2 align-justify">
        <img src="assets/img/blog.png">
    </div>
</div>
<?php include( 'footer.php' ); ?>

