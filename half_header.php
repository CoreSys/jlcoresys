<div class="header-wrapper">
    <div class="header">
        <?php include( 'header_nav.php' ); ?>
        <div class="bottom-wrapper hidden-phone">
            <div class="bottom">
                <div class="row-fluid">
                    <div class="span2 large-padding-top text-center hidden-tablet"><img src="img/header-bottom-left.png" /></div>
                    <div class="span8 text-center text-capitalize title2">
                        <h2>Do what you love <span>&</span> Success will follow</h2>
                        <h1>What We Can Do <span>For You</span></h1>
                    </div>
                    <div class="span2 large-padding-top text-center hidden-tablet"><img src="img/header-bottom-right.png" /></div>
                </div>
            </div>
        </div>
    </div>
    <div class="greyed text-center">
        <h2 class="small">HTML5 - CSS3 - PHP - MySQL - jQuery - Project Management - Consulting - Since 1998</h2>
    </div>
</div>