<?php $base = strstr( $_SERVER[ 'HTTP_HOST' ], 'localhost' ) || strstr( $_SERVER[ 'HTTP_HOST' ], '127.0.0.1' ) ? '/development/portfolio/' : '/'; ?>
<?php
$page = isset( $page ) ? $page : 'home';
$title = isset( $title ) ? $title : null;
$keywords = isset( $keywords ) ? $keywords : null;
if( !empty( $keywords ) ) { $keywords = ',' . $keywords; }
$css_files = isset( $css_files ) ? $css_files : array();
if( is_array( $css_files ) && count( $css_files )  > 0 )
{
    $css_files = ',' . implode( ',', $css_files );
} else {
    $css_files = null;
}
$head_js_files = isset( $head_js_files ) ? $head_js_files : array();
if( is_array( $head_js_files ) && count( $head_js_files )  > 0 )
{
    $head_js_files = ',' . implode( ',', $head_js_files );
} else {
    $head_js_files = null;
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>123J&L Core Systems <?php echo !empty( $title ) ? ' - ' . $title : ''; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="J&L Core Systems, experienced web design and development to suit all your needs. No job too hard, too small or too large.">
    <meta name="keywords" content="J&L Core Systems,symfony,zend,zend frameword,html5,css,js,javascript,jquery,extjs,wordpress,photoshop,victoria,bc,canada,design<?php echo $keywords; ?>">
    <meta name="author" content="J&L Core Systems">

    <!-- Styles -->
    <link href="css/mini.php?files=bootstrap.min,bootstrap-responsive.min,themes/default/default,nivo-slider,jlstyle<?php echo $css_files; ?>&t=<?php echo time(); ?>" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="js/mini.php?files=html5<?php echo $head_js_files; ?>"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="ico/favicon.png">

    <script type="text/javascript">function preloadImages(imgs){var picArr = [];for(var i = 0; i<imgs.length; i++){picArr[i]=new Image(100,100);picArr[i].src=imgs[i];}}preloadImages(['img/header-nav-item-home.jpg','img/header-nav-item-about.jpg','img/header-nav-item-portfolio.jpg','img/header-nav-item-services.jpg','img/header-nav-item-contact.jpg','img/header-bg2.gif','img/marker-left.png','img/marker-right.png','img/marker-left-active.png','img/marker-right-active.png']);</script>
</head>
<body data-spy="scroll">
<!--[if lt IE 7]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
<![endif]-->
<div class="site-wrapper">
