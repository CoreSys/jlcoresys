<?php
$body_js_files = isset( $body_js_files ) ? $body_js_files : array();
if( is_array( $body_js_files ) && count( $body_js_files )  > 0 )
{
    $body_js_files = ',' . implode( ',', $body_js_files );
} else {
    $body_js_files = null;
}
?>
<footer>
    <div class="footer">
        <div class="row1">
            <div class="column1 hidden-tablet hidden-phone"></div>
            <div class="column2"></div>
            <div class="column hidden-tablet hidden-phone"></div>
        </div>
        <div class="row2">
            <div class="column1 hidden-tablet hidden-phone"></div>
            <div class="column2 text-center"><h2>J&amp;L Core Systems</h2></div>
            <div class="column3 hidden-tablet hidden-phone"></div>
        </div>
        <div class="row3">
            <div class="column1 hidden-phone hidden-tablet">
                <div class="padding">
                    <ul class="list3">
                        <li><a href="<?php echo $base; ?>">Home</a></li>
                        <li><a href="<?php echo $base; ?>about">About</a></li>
                        <li><a href="<?php echo $base; ?>services">Services</a></li>
                        <li><a href="<?php echo $base; ?>portfolio">Portfolio</a></li>
                        <li><a href="<?php echo $base; ?>contact">Contact</a></li>
                    </ul>
                </div>
            </div>
            <div class="column2">
                <div class="copyright">
                    <div class="hidden-desktop">
                        <div style="float:left;width:49%" class="hidden-desktop">
                            <ul class="list3">
                                <li><a href="<?php echo $base; ?>">Home</a></li>
                                <li><a href="<?php echo $base; ?>about">About</a></li>
                                <li><a href="<?php echo $base; ?>services">Services</a></li>
                                <li><a href="<?php echo $base; ?>portfolio">Portfolio</a></li>
                                <li><a href="<?php echo $base; ?>contact">Contact</a></li>
                            </ul>
                        </div>
                        <div style="float:right;width:49%" class="hidden-desktop">
                            <ul class="list3">
                                <li><a href="<?php echo $base; ?>services#consulting">Expert PHP Consulting</a></li>
                                <li><a href="<?php echo $base; ?>services#project_management">Project Management</a></li>
                                <li><a href="<?php echo $base; ?>services#custom_applications">Custom Applications</a></li>
                                <li><a href="<?php echo $base; ?>services#addons">Add-On Development</a></li>
                                <li><a href="<?php echo $base; ?>services#design">Graphic Design</a></li>
                                <li><a href="<?php echo $base; ?>services#web_development">Web Development</a></li>
                            </ul>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="hidden-phone hidden-tablet"><br><br><br><br></div>
                    <small>Copyright &copy; 1998-2013 J&amp;L Core Systems. All rights reserved<br>Located in Victoria BC, Canada</small>
                </div>
            </div>
            <div class="column3 hidden-phone hidden-tablet">
                <div class="padding">
                    <ul class="list3">
                        <li><a href="<?php echo $base; ?>services#consulting">Expert PHP Consulting</a></li>
                        <li><a href="<?php echo $base; ?>services#project_management">Project Management</a></li>
                        <li><a href="<?php echo $base; ?>services#custom_applications">Custom Applications</a></li>
                        <li><a href="<?php echo $base; ?>services#addons">Add-On Development</a></li>
                        <li><a href="<?php echo $base; ?>services#design">Graphic Design</a></li>
                        <li><a href="<?php echo $base; ?>services#web_development">Web Development</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
</div>
<script type="text/javascript" src="js/mini.php?files=jquery,cufon-yui,Copse_400.font,site<?php echo $body_js_files; ?>"></script>
</body>
</html>