<?php $base = strstr( $_SERVER[ 'HTTP_HOST' ], 'localhost' ) || strstr( $_SERVER[ 'HTTP_HOST' ], '127.0.0.1' ) ? '/development/portfolio/' : '/'; ?>
<?php ob_start(); ?>
<?php $head_js_files = array( ); ?>
<?php $body_js_files = array( 'bootstrap-modal','bootstrap-tab','jquery.imagesloaded','jquery.wookmark.min','jquery.fancybox.pack','samples' ); ?>
<?php $css_files = array( 'jquery.fancybox','sample' ); ?>
<?php $page = 'portfolio'; ?>
<?php $keywords = 'portfolio,our work,past work,clients'; ?>
<?php $title = 'Portfolio'; ?>
    <div class="content-wrapper">
        <section class="row-fluid">
            <div class="text-center title2">
                <h2>Due to Client Confidentiality, some clients have not been included on this page.</h2>
            </div>
            <?php
                $clients = array(
                    'MK2 Business Solutions',
                    'Adventure Tech',
                    'W**G*** Productions',
                    'CanamWalking (Circle Canada)',
                    'Craig Walters Group',
                    'Action Motorcycles',
                    'International Alpacas',
                    'B*****',
                    'S***** E****',
                    'C***** S***',
                    'Speakwell',
                    'Sawyer &amp; Sawyer',
                    'Wilderness Trekking',
                    'Zensport',
                    'BC Provincial Government',
                    'DigitalWave Solution',
                    'Oops Demolition',
                    'Jenny McGuire',
                    'Netdegree',
                    'TravelAce',
                    'Bargain',
                    'AdCore AdServer',
                    'Shermans Travel (Bargain/TravelAce)'
                );
                sort( $clients );
            ?>
            <ul class="list3 half">
                <?php foreach( $clients as $client ): ?>
                <li><?php echo $client; ?></li>
                <?php endforeach; ?>
            </ul>
            <div class="clear"></div>
            <br>
                <div id="sample relative samples-container" class="samples">
                        <ul id="tiles">
                        <?php listSamples(); ?>
                        </ul>
                    <div class="clear"></div>
                </div>
            <div class="clear"></div>
        </section>
    </div>
<?php
function listSamples(){
    $folder = dirname( __FILE__ );
    $folder = $folder . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR . 'samples';
    $dir = opendir( $folder );
    $count = 1;
    if( $dir )
    {
        while( false !== ( $file = readdir( $dir ) ) )
        {
            $filename = $folder . DIRECTORY_SEPARATOR . $file;
            if( !is_dir( $filename ) && is_file( $filename ) )
            {
                if( $file != '.' && $file != '..' )
                {
                    $url = 'img/samples/thumbs/' . $file;
                    $tar = 'img/samples/' . $file;
//                    echo '<div class="element"><p class="number hidden">' . $count . '</p><img src="' . $url . '" width="300" /></div>';
                    echo '<li><a href="' . $tar . '" class="fancybox" rel="samples"><img src="' . $url . '"></a></li>';
                    $count++;
                }
            }
        }

        closedir( $dir );
    }
}
?>
<?php $contents = ob_get_clean(); ?>
<?php include( 'header.php' ); ?>
<?php include( 'half_header.php' ); ?>
<?php echo $contents; ?>
<?php include( 'footer.php' ); ?>